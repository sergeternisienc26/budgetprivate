import * as React from "react";
import { View, Text, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Home from "./screens/home";
import Login from "./screens/login";
import Register from "./screens/register";
import Spends from "./screens/spends";
import SpendsApiscreen from "./screens/api/spendsApi";
import UserApiscreen from "./screens/api/userApi";
import TypeApiscreen from "./screens/api/typeApi";
import AddSpendsscreen from "./screens/addSpends";
import lastMonthSpend from "./screens/lastMonth";
import SpendsbyTypescreen from "./screens/spendsByType";
import SpendsLastMonthscreen from "./screens/spendsLastMonth";
import SpendsMonthYearscreen from "./screens/spendsMonthYear";


const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="home" component={Home} />
        <Stack.Screen name="login" component={Login} />
        <Stack.Screen name="register" component={Register} />
        <Stack.Screen name="spends" component={Spends} />
        <Stack.Screen name="apiSpends" component={SpendsApiscreen} />
        <Stack.Screen name="apiUser" component={UserApiscreen} />
        <Stack.Screen name="apiType" component={TypeApiscreen} />
        <Stack.Screen name="addSpend" component={AddSpendsscreen} />
        <Stack.Screen name="lastMonth" component={lastMonthSpend} />
        <Stack.Screen name="spendsByType" component={SpendsbyTypescreen} />
        <Stack.Screen name="spendsLastMonth" component={SpendsLastMonthscreen} />
        <Stack.Screen name="spendsMonthYear" component={SpendsMonthYearscreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
