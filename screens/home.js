import * as React from "react";
import { Image, View, Text, Button, StyleSheet } from "react-native";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


const Tab = createBottomTabNavigator();

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#01001a",
      justifyContent: "center",
      alignItems: "center",
    },
    TextTitles: {
      color: "#ffffff",
      fontSize: 36,
    },
});

export default function HomeScreen({ navigation }) {
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.TextTitles}>Budget Private</Text>
        <Button
            title="Se connecter"
            onPress={() => navigation.navigate("login")}
          />
          <Button
            title="S'inscrire"
            onPress={() => navigation.navigate("register")}
          />
          <Button
            title="Mes dépenses"
            onPress={() => navigation.navigate("spends")}
          />
          
      </View>

      
    </>
  );
}