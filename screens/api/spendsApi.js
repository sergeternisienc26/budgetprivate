import * as React from 'react';
import react, { useEffect, useState } from 'react';
import { StyleSheet, Text, View,FlatList, ActivityIndicator } from 'react-native';
import { urlApi } from './api';
import axios from 'axios';


export default function SpendsApiscreen ({navigation}) {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const urlApiSpends = urlApi + 'api/spends?page=1';

  function axiosApiCall() {
  
  axios.get(urlApiSpends)
  .then(function (response) {
     setLoading(false);
     setData(response.data['hydra:member']);
    console.log(response.data);
  })
  .catch(function (error) {
   
    console.log(error);
  })
  
}

  useEffect(() => {
    axiosApiCall();
  }, []);




  return (
    <>
    <View style={{ padding: 24 }}>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList
          data={data}
          keyExtractor={item => item.id}
          renderItem={({ item }) => (
            <Text>{item.name}, {item.description},  {item.amount}</Text>
          )}
        />
      )}
    </View>
    </>
  );
};