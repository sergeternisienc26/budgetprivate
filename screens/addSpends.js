import * as React from "react";
import react, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  FlatList,
  ActivityIndicator,
  TextInput,
  Button,
  Picker,
} from "react-native";
import { urlApi } from "../screens/api/api";
import axios from "axios";
import { Formik } from "formik";

export default function AddSpendsscreen() {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const urlApiTypes = urlApi + "api/types?page=1";
  const [selectedValue, setSelectedValue] = useState("lop");

  const  [newspend, setNewspend] = useState({});

  function axiosApiCall() {
    axios
      .get(urlApiTypes)
      .then(function (response) {
        setLoading(false);
        setData(response.data["hydra:member"]);
        console.log(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  useEffect(() => {
    axiosApiCall();
  }, []);


  useEffect(()=> {
    console.log('hohoh');
   
    const spendNew = {
      "name": newspend.name,
      "description": newspend.description,
      "amount": newspend.amount,
      "createdAt": new Date
    }; 

    // axios.post('http://127.0.0.1:8000/api/spends', {
    //   "name": newspend.name,
    //   "description": newspend.description,
    //   "amount": newspend.amount,
    //   "createdAt": new Date
    // })
    // .then(function (response) {
    //   console.log(response);
    // })
    // .catch(function (error) {
    //   console.log(error);
    // });
    
    console.log({newspend,selectedValue,spendNew} ); 

  }, [newspend]);




  return (
    <>
      <ScrollView>
        <View style={{ padding: 24 }}>
          <Formik
            initialValues={{ name: "", description: "", amount:""}}
            onSubmit={(values) => {setNewspend(values)}}
            
          >
            {({ handleChange, handleBlur, handleSubmit, values }) => (
              <View>
                <TextInput
                  label="Nom Depense"
                  placeholder="Depense"
                  onChangeText={handleChange("name")}
                  onBlur={handleBlur("name")}
                  value={values.name}
                />
                <TextInput
                  label="description"
                  placeholder="description"
                  onChangeText={handleChange("description")}
                  onBlur={handleBlur("description")}
                  value={values.description}
                />
                <TextInput
                  label="montant"
                  placeholder="montant"
                  onChangeText={handleChange("amount")}
                  onBlur={handleBlur("amount")}
                  value={values.amount}
                />
               <View style={{ padding: 24 }}>
            {isLoading ? (
              <ActivityIndicator />
            ) : (
              <Picker
              selectedValue={selectedValue}
              style={{ height: 50, width: 150 }}
              onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
            >
              {/* <Picker.Item label="Java" value="java" /> */}
              { data.length > 0 && data.map((item) => {
                return (<Picker.Item label={item.name} value={item.id}  />)
              })  }
            </Picker>
              // <FlatList
              //   data={data}
              //   keyExtractor={({ id }, index) => id}
              //   renderItem={({ item }) => (
              //     <Text>
              //       {item.name}, {item.description}
              //     </Text>
              //   )}
              // />
            )}
          </View>

                <Button onPress={handleSubmit} title="Submit" />
              </View>
            )}
          </Formik>

          <View style={{ padding: 24 }}>
            {isLoading ? (
              <ActivityIndicator />
            ) : (
              <FlatList
                data={data}
                keyExtractor={({ id }, index) => id}
                renderItem={({ item }) => (
                  <Text>
                    {item.name}, {item.description}, {item.id}
                  </Text>
                )}
              />
            )}
          </View>
        </View>
      </ScrollView>
    </>
  );
}
