import * as React from "react";
import react, { useState, useEffect } from "react";
import { View,Text,ScrollView,FlatList,TouchableOpacity, Image, ActivityIndicator,Button} from "react-native";
import { urlApi } from '../screens/api/api';
import axios from 'axios';



export default function SpendsLastMonthscreen({navigation}) {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const urlApiSpends = urlApi + 'api/user/38/lastmonth';

  function axiosApiCall() {
  
  axios.get(urlApiSpends)
  .then(function (response) {
     setLoading(false);
     setData(response.data);
    console.log(response.data);
  })
  .catch(function (error) {
   
    console.log(error);
  })
  
}

  useEffect(() => {
    axiosApiCall();
  }, []);

  return (
    <>
      <ScrollView>  
             
      <Button
          title="ajouter une dépense"
          onPress={() => navigation.navigate("addSpend")}
        />

      <Button
          title="dépenses par catégorie"
          onPress={() => navigation.navigate("spendsByType")}
        />

      <Button
          title="dépenses Mois par Mois"
          onPress={() => navigation.navigate("spendsMonthYear")}
        />
   
      <View style={{ padding: 24 }}>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList
          data={data}
          keyExtractor={item => item.id}
          renderItem={({ item }) => (
            <View style={{padding: 12}}> 
            <Text>Date: {item.createdAt} </Text>
            <Text>Nom dépense: {item.name}</Text>
            <Text>Description: {item.description}</Text>
           <Text>Montant: {item.amount}</Text>
            </View>
          )}
        />
      )}
    </View>
      </ScrollView>
    </>
  );
}