import React from 'react';
import { useState } from 'react'
import { View, Text, Button } from 'react-native';
import { createAccount } from '../api/mock';
import { setToken } from '../api/token';

  const CreateAccount = ({ navigation }) => {
    const [errorMessage, setErrorMessage] = useState('');
    const createUser = async () => {
      createAccount('test@test.ca', 'password')
      setErrorMessage('');
      login('test@test.ca', 'password')
        .then(async (res) => {
          await setToken(res.auth_token);
          navigation.navigate('Home');
        })
        .catch((err) => setErrorMessage('Error', err.message));
    };

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>CreateAccount</Text>
      <Button title="Create user" onPress={createUser} />
      <Button title="Log in" onPress={() => navigation.navigate('LoginScreen')} />
      {errorMessage ? <Text>{errorMessage}</Text> : null}
    </View>
  );
};
export default CreateAccount;
