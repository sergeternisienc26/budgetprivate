import { getToken } from './token';

const mockSuccess = (value) => {
    return new Promise((resolve) => {
      setTimeout(() => resolve(value), 2000);
    });
  };
  
  const mockFailure = (value) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => reject(value), 2000);
    });
  };

/* Méthode de connexion */

export const login = (email, password, shouldSucceed = true) => {
    console.log(email, password);
  
    if (!shouldSucceed) {
      return mockFailure({ error: 500, message: 'Something went wrong!' });
    }
  
    return mockSuccess({ auth_token: 'successful_fake_token' });
  };


/* Méthode de création d'un utilisateur */

export const createAccount = (email, password, shouldSucceed = true) => {
console.log(email, password);

if (!shouldSucceed) {
return mockFailure({ error: 500, message: 'Something went wrong!' });
}

return mockSuccess({ auth_token: 'successful_fake_token' });
}

const getAuthenticationToken = () => 'successful_fake_token';



/* Récupération de nos utilisateur */

export const getUsers = async (shouldSucceed = true) => {
  const token = await getToken();

  if (token !== 'successful_fake_token') {
    return mockFailure({ error: 401, message: 'Invalid Request' });
  }

  return mockSuccess({
    users: [
      {
        email: 'test@test.ca',
      },
      {
        email: 'test2@test.ca',
      },
    ],
  });
};